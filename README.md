# jo

jo is short for JSON Object. Use jo to retrieve struct and map values by string
paths. jo provides sugar functions to convert values.

To install

```sh
go get gitlab.com/mgutz/jo
```

## Get Example

```go
o, _ := jo.FromString(`{
  "a": 1,
  "b": "cow",
  "c": 1.2,
  "d": {
    "e": true,
    "f": false,
    "g": {
      "h": [0,1,2,3],
      "i": [
        {"j": 1},
        {"k.cow": "Moo"}
      ]
    }
  }
}`)

a := o.AsString("a")
fmt.Printf("a (%T) = %v\n", a, a)

c := o.AsFloat("c")
fmt.Printf("c (%T) = %v\n", c, c)

de := o.AsBool("d.e")
fmt.Printf("d.e (%T) = %v\n", de, de)

dg := o.AsMap("d.g")
fmt.Printf("d.g (%T) = %v\n", dg, dg)

dgh1 := o.AsInt("d.g.h[1]")
fmt.Printf("d.g.h[1] (%T) = %v\n", dgh1, dgh1)

dgi1k := o.AsString(`d.g.i[1]["k.cow"]`)
fmt.Printf(`d.g.i[1]["k.cow"] (%T) = %v\n`, dgi1k, dgi1k)
```

prints

```
a (int) = 1
c (float64) = 1.2
d.e (bool) = true
d.g (map[string]interface {}) = map[h:[0 1 2 3] i:[map[j:1] map[k:Moo]]]
d.g.h[1] (int) = 1
d.g.i[1]["k.cow"] (string) = Moo
```

## Set Example

```go
o := jo.New()

o.Set("a.b.c", 1)
o.Set("a.b.d", []interface{}{1, 2, 3})
o.Set("a.b.e", map[string]interface{}{
  "f": "Hello", "g": "World",
})
o.Set("a.b.e.g", "Universe")
o.Set("a.b.d[0]", 6.9)

jsonStr, _ := json.PrettyStringify()
fmt.Println(jsonStr)
```

prints

```json
{
  "a": {
    "b": {
      "c": 1,
      "d": [6.9, 2, 3],
      "e": {
        "f": "Hello",
        "g": "Universe"
      }
    }
  }
}
```

## Credit

`jo` was originally a port from [nestedjson](https://github.com/wenxiang/go-nestedjson).
The main difference is `jo` is loosely typed and allows conversion to any type.
For example, a `Float64` can be retrieved as a `string`.

I created a version before go modules [jo](https://github.com/mgutz/jo)

## LICENSE

MIT
