module gitlab.com/mgutz/jo

go 1.18

require (
	github.com/mgutz/to v1.0.0
	github.com/stretchr/testify v1.8.0
)

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	gitlab.com/mgutz/objpath v0.0.0-20221002052525-c882e035af6a // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
