package jo

import (
	"encoding/json"
	"fmt"
	"io"
	"os"
	"os/user"
	"strings"

	"gitlab.com/mgutz/objpath"
)

const mustFormat = "Path not found or could not convert to %s"

// Object is a generic map.
type Object struct {
	data interface{}
}

func getPart(obj interface{}, part interface{}, createMissingObject bool) (interface{}, error) {
	value, err := objpath.Lookup(obj, part)
	if err == nil {
		return value.Interface(), nil
	}

	switch p := part.(type) {
	case int:
		return nil, fmt.Errorf("%s is not an array: %T", obj, obj)

	case string:
		if m, ok := obj.(map[string]interface{}); ok {
			if createMissingObject {
				rv := make(map[string]interface{})
				m[p] = rv
				return rv, nil
			}
		}
		return nil, fmt.Errorf("%s is not an object: %T", obj, obj)
	}
	return nil, fmt.Errorf("invalid sub path: %T", part)
}

// New creates a new JSON object struct.
func New() *Object {
	return &Object{make(map[string]interface{})}
}

// FromAny creates a new Object using `v` directly.
func FromAny(v interface{}) (*Object, error) {
	if v == nil {
		return nil, fmt.Errorf("v is nil")
	}
	return &Object{v}, nil
}

// FromJSONFile creates a new Object from a filename. filename can be prefixed
// with ~ for home directory.
func FromJSONFile(filename string) (*Object, error) {
	fname, err := untildify(filename)
	if err != nil {
		return nil, err
	}

	file, err := os.Open(fname)
	if err != nil {
		return nil, err
	}

	return FromReadCloser(file)
}

// FromReadCloser decodes JSON from an io.ReadCloser
// suchs as Request.Body and Response.Body and closes it.
func FromReadCloser(body io.ReadCloser) (*Object, error) {
	decoder := json.NewDecoder(body)
	var result map[string]interface{}
	err := decoder.Decode(&result)
	if err != nil {
		body.Close()
		return nil, err
	}
	err = body.Close()
	return &Object{result}, err
}

// FromBytes creates an object directly from bytes.
func FromBytes(b []byte) (*Object, error) {
	obj := New()
	err := obj.UnmarshalJSON(b)
	return obj, err
}

// FromString creates an object directly from a string.
func FromString(json string) (*Object, error) {
	obj := New()
	err := obj.UnmarshalJSON([]byte(json))
	return obj, err
}

// PrettyStringify returns the JSON indented representation of this map.
func (n *Object) PrettyStringify() string {
	b, err := json.MarshalIndent(n.data, "", "  ")
	if err != nil {
		return "nil"
	}
	return string(b)
}

// Stringify returns the JSON string representation of this map.
func (n *Object) Stringify() string {
	b, err := n.MarshalJSON()
	if err != nil {
		return "nil"
	}
	return string(b)
}

func get(v interface{}, path string) (interface{}, error) {
	value, err := objpath.Get(v, path)
	if err != nil {
		return nil, err
	}
	return value.Interface(), nil
}

// Get gets value at path, eg x["key].someKey[0]
func (n *Object) Get(path string) (interface{}, error) {
	if path == "." {
		return n.data, nil
	}
	return get(n.data, path)

}

// Set sets the value at path.
func (n *Object) Set(path string, val interface{}) error {
	parts, err := objpath.SplitPath(path, `"`)
	if err != nil {
		return err
	}

	curr := n.data
	for _, part := range parts[:len(parts)-1] {
		curr, err = getPart(curr, part, true)
		if err != nil {
			return err
		}

	}

	switch k := parts[len(parts)-1].(type) {
	case int:
		if arr, ok := curr.([]interface{}); ok {
			arr[k] = val
		} else {
			return fmt.Errorf("Not an array: %s", curr)
		}

	case string:
		if m, ok := curr.(map[string]interface{}); ok {
			m[k] = val
		} else {
			return fmt.Errorf("Not an object: %#v", parts)
		}
	}

	return nil
}

// UnmarshalJSON implements Unmarshaller interface.
func (n *Object) UnmarshalJSON(b []byte) error {
	return json.Unmarshal(b, &n.data)
}

// MarshalJSON implements Marshaller interface.
func (n *Object) MarshalJSON() ([]byte, error) {
	return json.Marshal(n.data)
}

// Data returns the entire data map.
func (n *Object) Data() interface{} {
	return n.data
}

// Sets data, the internal indexed object for all methods.
func (n *Object) SetData(o interface{}) {
	n.data = o
}

// untildify replaces leading ~ with current user's home directory
func untildify(path string) (string, error) {
	if !strings.HasPrefix(path, "~") {
		return path, nil
	}

	currentUser, err := user.Current()
	if err != nil {
		return "", err
	}

	if strings.HasPrefix(path, "~") {
		return currentUser.HomeDir + path[1:], nil
	}
	return path, nil
}
